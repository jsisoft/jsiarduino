/*
  Button & LED Test
  Turns on an LED on Pin 13 when starting, then rotates between 11, 12, 13 when pressing a button.
  This example code is in the public domain.
 */
int button = 2;
int state = 0;

void setup() {                
  // initialize the digital pin as an output.
  for(int i = 8; i < 14; i++)
  {
    pinMode(i, OUTPUT);
  }
  
  // Pin 2 is connected to a button tied to GND with pullup 10K to VCC.
  pinMode(button, INPUT);
  
}

void loop() {

  if (digitalRead(button) == LOW && (state == 0)) 
  {
    digitalWrite(13, HIGH);
    state = 1;
    delay(150);
    while (digitalRead(button) == LOW);
    
    delay(150);
  }
  if(state > 0)
  {

    if(digitalRead(button) == LOW)
    {
      digitalWrite(13, HIGH);
      delay(150);
      while (digitalRead(button) == LOW);    
      delay(150);
      state = 0;
      return;
    }
  
  for(int i = 8; i < 14; i++)
  {
    digitalWrite(i, (i == state));
  }
  state++;  
  if(state > 13) state = 1;
  
  delay(150);
  }
  
}
